import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import {ENV} from './store/config'

import Notifications from 'vue-notification'
import VueSocketio from 'vue-socket.io'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import moment from 'moment'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faClock, faCheck } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.filter('humanReadableTime', function (value) {
  const dateFormat = new Date(parseInt(value))
  return moment(dateFormat).fromNow()
})

library.add(faClock, faCheck)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(Notifications)
Vue.use(VueSocketio, ENV.API_URL)
Vue.use(ElementUI)

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
